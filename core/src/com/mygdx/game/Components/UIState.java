package com.mygdx.game.Components;

public abstract class UIState {
    public enum UITextAlignment{
        ALIGNMENT_LEFT, ALIGNMENT_RIGHT, ALIGNMENT_CENTER
    }

    public enum UIUnit {
        UNIT_PERCENT, UNIT_PIXEL;
    }
}
