package com.mygdx.game.Components;

import java.util.ArrayList;
import java.util.Collections;

public class UIMouseOverComponent extends UIEventComponents{

    public UIMouseOverComponent(UIComponent... c){
        components = new ArrayList<>();
        Collections.addAll(components, c);
    }

}
