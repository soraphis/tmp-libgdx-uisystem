package com.mygdx.game.Components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class UIFontComponent extends UIElements {

    public BitmapFont   font;

    public UIFontComponent(BitmapFont font){
        this.font = font;
    }

    public UIFontComponent(BitmapFont font, Color fontColor){
        this.font = font;
        this.font.setColor(fontColor);
    }

    public UIFontComponent(BitmapFont font, Color fontColor, int size){
        this.font = font;
        this.font.setColor(fontColor);
        this.font.setScale(size);
    }

}
