package com.mygdx.game.Components;

import java.util.ArrayList;
import java.util.Collections;

public class UIMouseOutComponent extends UIEventComponents{

    public boolean over = false;

    public UIMouseOutComponent(UIComponent... c){
        components = new ArrayList<>();
        Collections.addAll(components, c);
    }

}