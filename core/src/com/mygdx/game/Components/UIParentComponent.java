package com.mygdx.game.Components;

import com.badlogic.ashley.core.Entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UIParentComponent extends UIElements{


    private ArrayList<Entity> mChilds = new ArrayList<>();

    public void addChild(Entity e){
        if(e.getComponent(UIChildComponent.class) == null )
            System.err.println("missing UIChildComponent");
        mChilds.add(e);
    }

    final public List<Entity> getChilds(){ return Collections.unmodifiableList(mChilds); }

}
