package com.mygdx.game.Components;

public class UILabelComponent extends UIElements {

    public String text;
    public UIState.UITextAlignment textAlignment = UIState.UITextAlignment.ALIGNMENT_LEFT;

    public UILabelComponent(String text){
        this.text = text;
    }

    public UILabelComponent(String text, UIState.UITextAlignment alignment){
        this(text);
        this.textAlignment = alignment;
    }
}
