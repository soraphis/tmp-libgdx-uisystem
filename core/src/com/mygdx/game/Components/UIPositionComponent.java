package com.mygdx.game.Components;

import com.badlogic.gdx.math.Vector2;

public class UIPositionComponent extends UIElements{

    public Vector2 position;
    public Vector2 renderedPosition;
    public UIState.UIUnit unit;

    /**
     * @param x horizontal position of the element, from left to right
     * @param y vertical position of the element, from _bottom_ to top
     * @param unit pixel or percentual(default) positioning based on the parent element
     */
    public UIPositionComponent(float x, float y, UIState.UIUnit unit){
        this(x, y);
        this.unit = unit;
    }

    public UIPositionComponent(float x, float y){
        position = new Vector2(x, y);
        renderedPosition = new Vector2();
        unit = UIState.UIUnit.UNIT_PERCENT;
    }


}
