package com.mygdx.game.Components;

import com.badlogic.gdx.math.Vector2;

public class UIDimensionComponent extends UIElements{

    public Vector2 dimension;
    public Vector2 renderedDimension;
    public UIState.UIUnit unit;


    /**
     * @param width of the element
     * @param height height of the element
     * @param unit pixel or percentual(default) positioning based on the parent element
     */
    public UIDimensionComponent(float width, float height, UIState.UIUnit unit){
        this(width, height);
        this.unit = unit;
    }

    public UIDimensionComponent(float width, float height){
        dimension = new Vector2(width, height);
        renderedDimension = new Vector2();
        unit = UIState.UIUnit.UNIT_PERCENT;
    }

}
