package com.mygdx.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Color;
import com.mygdx.game.Components.*;

public class UIBuilder {

    private Entity mEntity;

    public UIBuilder(Engine engine){
        mEntity = new Entity();
        engine.addEntity(mEntity);
    }

    public Entity build(){              return mEntity;     }

    public UIBuilder pos(float x, float y){
        mEntity.add(new UIPositionComponent(x, y));         return this;  }
    public UIBuilder pos(float x, float y, UIState.UIUnit unit){
        mEntity.add(new UIPositionComponent(x, y, unit));   return this;  }

    public UIBuilder dim(float w, float h){
        mEntity.add(new UIDimensionComponent(w, h));        return this;  }
    public UIBuilder dim(float w, float h, UIState.UIUnit unit){
        mEntity.add(new UIDimensionComponent(w, h, unit));  return this;  }

    public UIBuilder parent(){ mEntity.add(new UIParentComponent());  return this;  }
    public UIBuilder parent(UIParentComponent parent){ mEntity.add(parent);  return this;  }
    public UIBuilder child(){  mEntity.add(new UIChildComponent());  return this;  }

    public UIBuilder label(String s){ mEntity.add(new UILabelComponent(s)); return this;}
    public UIBuilder label(String s, UIState.UITextAlignment st) {
        mEntity.add(new UILabelComponent(s, st)); return this;
    }

    public UIBuilder background(Color c){
        mEntity.add(new UIBackgroundComponent(c)); return this;
    }


//    public UIBuilder basedOf(UIArchetype archetype){
//        archetype.components.forEach(mEntity::add);
//        return this;
//    }
}
