package com.mygdx.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.mygdx.game.Components.*;
import com.mygdx.game.Systems.UIInputSystem;
import com.mygdx.game.Systems.UIRenderSystem;


public class Menu implements Screen {

    final MyGdxGame game;
    final Engine engine = new Engine();

    public Menu(final MyGdxGame game){
        this.game = game;

        Gdx.input.setInputProcessor(new UIInputSystem(engine));

        new UIBuilder(engine).build();

        // Create Parent Canvas Entity
        UIParentComponent parent = new UIParentComponent();
        Entity canvas = new UIBuilder(engine).parent(parent).build();

        UIParentComponent panel = new UIParentComponent();
        // Create Child (label) component
        parent.addChild(
                new UIBuilder(engine)
                        .child() // maybe not necessary
                        .parent(panel)
                        .pos(10, 10, UIState.UIUnit.UNIT_PIXEL)
                        .dim(20, 80)
                        .background(Color.RED)
                        .build()
                .add(new UIMouseOverComponent(new UIBackgroundComponent(Color.GREEN)))
                .add(new UIMouseOutComponent(new UIBackgroundComponent(Color.RED)))
        );

        parent.addChild(
                new UIBuilder(engine)
                        .child()
                        .pos(80, 80)
                        .dim(50, 50, UIState.UIUnit.UNIT_PIXEL)
                        .background(Color.BLUE)
                        .build()
                        .add(new UIMouseOverComponent(new UIBackgroundComponent(Color.YELLOW)))
                        .add(new UIMouseOutComponent(new UIBackgroundComponent(Color.BLUE)))
        );

        // copy of the above but different parent
        panel.addChild(
                new UIBuilder(engine)
                        .child()
                        .pos(80, 80)
                        .dim(50, 50, UIState.UIUnit.UNIT_PIXEL)
                        .background(Color.BLUE)
                        .build()
                        .add(new UIMouseOverComponent(new UIBackgroundComponent(Color.YELLOW)))
                        .add(new UIMouseOutComponent(new UIBackgroundComponent(Color.BLUE)))
        );
        // Create Systems
        new UIInputSystem(engine);
        engine.addSystem(new UIRenderSystem(canvas));
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        engine.update(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
