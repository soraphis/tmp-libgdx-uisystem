package com.mygdx.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.mygdx.game.Components.*;

@Deprecated
public abstract class UIFactory {

    public static Engine engine = new Engine();

    public static Entity constructCanvas(UIParentComponent parent){
        return new UIBuilder(engine)
                .parent(parent)
                .build();
    }

    public static Entity constructLabel(String label, float x, float y){
        return constructLabel(label, x, y, UIState.UITextAlignment.ALIGNMENT_LEFT, Color.WHITE);
    }

    public static Entity constructLabel(String label, float x, float y, UIState.UITextAlignment alignment){
        return constructLabel(label, x, y, alignment, Color.WHITE);
    }

    public static Entity constructLabel(String label, float x, float y, UIState.UITextAlignment alignment, Color c){
        return new UIBuilder(engine).pos(x, y)
                .label(label, alignment).build()
                .add(new UIFontComponent(new BitmapFont(), c));
    }

    public static Entity constructPanel(UIParentComponent parent, float x, float y, float dx, float dy){
        return new UIBuilder(engine)
                .pos(x, y).dim(dx, dy)
                .child().parent(parent).build();
    }

}
