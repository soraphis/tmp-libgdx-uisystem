package com.mygdx.game.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Components.*;

public class UIInputSystem implements InputProcessor {

    Engine engine;

    private boolean isPointinArea(final Vector2 point, final Vector2 areaStart, final Vector2 areaDimension){
        Vector2 pos = new Vector2(point);
        pos.sub(areaStart);
        if(!(pos.x >= 0 && pos.y >= 0)) return false;
        if(!(pos.x <= areaDimension.x && pos.y <= areaDimension.y)) return false;
        return true;
    }



    public UIInputSystem(Engine engine){
        this.engine = engine;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        screenY = Gdx.graphics.getHeight() - screenY; // because libgdx is dump as shit

        Family f = Family.one(UIMouseOverComponent.class, UIMouseOutComponent.class).get();
        ImmutableArray<Entity> entities = engine.getEntitiesFor(f);

        for(Entity e : entities) {
            UIPositionComponent p = e.getComponent(UIPositionComponent.class);
            UIDimensionComponent d = e.getComponent(UIDimensionComponent.class);
            if (p == null || d == null) continue;
            if (isPointinArea(new Vector2(screenX, screenY), p.renderedPosition, d.renderedDimension)) {
                if (e.getComponent(UIMouseOverComponent.class) != null)
                    e.getComponent(UIMouseOverComponent.class).components.forEach(e::add);
                if (e.getComponent(UIMouseOutComponent.class) != null)
                    e.getComponent(UIMouseOutComponent.class).over = true;
            } else {
                if (e.getComponent(UIMouseOutComponent.class) != null &&
                        e.getComponent(UIMouseOutComponent.class).over) {
                    e.getComponent(UIMouseOutComponent.class).components.forEach(e::add);
                    e.getComponent(UIMouseOutComponent.class).over = false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
