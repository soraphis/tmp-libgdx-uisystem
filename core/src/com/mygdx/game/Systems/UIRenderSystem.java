package com.mygdx.game.Systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Components.*;

public class UIRenderSystem extends EntitySystem {

    private SpriteBatch batch;
    Entity canvas;


    /**
     * the canvas is the node of every UI screen
     */
    public UIRenderSystem(Entity canvas){
        this.canvas = canvas;
        batch = new SpriteBatch();
    }

    //private void renderChilds(Entity parent){ this.renderChilds(parent, new Vector2(), new Vector2()); }


    /**
     * @param parent parent element
     * @param ppos parent position in px
     * @param pdim parent dimension in px
     */
    private void renderChilds(final Entity parent, final Vector2 ppos, final Vector2 pdim){
        // check for valid parent:
        if(parent.getComponent(UIParentComponent.class) == null)    throw new IllegalArgumentException();

        for(Entity e : parent.getComponent(UIParentComponent.class).getChilds()){
            Vector2 cpos = null, cdim = null; /* define child position and dimension */
            if(e.getComponent(UIPositionComponent.class) != null){
                cpos = new Vector2(e.getComponent(UIPositionComponent.class).position);
                if(e.getComponent(UIPositionComponent.class).unit == UIState.UIUnit.UNIT_PIXEL){
                    cpos.add(ppos);
                }else {
                    cpos.scl(0.01f);
                    cpos = new Vector2(pdim.x * cpos.x, pdim.y * cpos.y).add(ppos);
                }
                e.getComponent(UIPositionComponent.class).renderedPosition = cpos;
            }
            if(e.getComponent(UIDimensionComponent.class) != null){
                cdim = new Vector2(e.getComponent(UIDimensionComponent.class).dimension);
                if(e.getComponent(UIDimensionComponent.class).unit == UIState.UIUnit.UNIT_PERCENT){
                    cdim.scl(0.01f);
                    cdim = new Vector2(pdim.x * cdim.x, pdim.y * cdim.y);
                }
                e.getComponent(UIDimensionComponent.class).renderedDimension = cdim;
            }

            UIComponent mainComponent = e.getComponent(UIBackgroundComponent.class);
            if(mainComponent != null)
                renderField(cpos.x, cpos.y, cdim.x, cdim.y, ((UIBackgroundComponent) mainComponent).backgroundColor);

            mainComponent = e.getComponent(UILabelComponent.class);
            if(mainComponent != null){
                UIFontComponent fc = e.getComponent(UIFontComponent.class);
                UIState.UITextAlignment t = ((UILabelComponent) mainComponent).textAlignment;
                float x = cpos.x;
                if(t == UIState.UITextAlignment.ALIGNMENT_RIGHT)
                    x -= fc.font.getBounds(((UILabelComponent)mainComponent).text).width;
                else if(t == UIState.UITextAlignment.ALIGNMENT_CENTER)
                    x -= fc.font.getBounds(((UILabelComponent)mainComponent).text).width / 2.0;

                fc.font.draw(batch, ((UILabelComponent)mainComponent).text, x, cpos.y);
            }
            /*
            mainComponent = e.getComponent(UILabelComponent.class);
            if(mainComponent != null){
                // there is a label to be rendered
                UIFontComponent fc = e.getComponent(UIFontComponent.class);
                Vector2 pos = e.getComponent(UIPositionComponent.class).position;
                UIText.UITextAlignment t = ((UILabelComponent) mainComponent).textAlignment;
                float x = ppos.x +  pos.x * pdim.x / 100,
                        y = ppos.y +  pos.y * pdim.y / 100;
                if(t == UIText.UITextAlignment.ALIGNMENT_CENTER){
                    x -= fc.font.getBounds(((UILabelComponent)mainComponent).text).width / 2;
                }else if(t == UIText.UITextAlignment.ALIGNMENT_RIGHT){
                    x -= fc.font.getBounds(((UILabelComponent)mainComponent).text).width;
                }
                fc.font.draw(batch, ((UILabelComponent)mainComponent).text, x, y);

                pos.add(ppos);
            }
            */

            if( e.getComponent(UIParentComponent.class) != null) {
                if(e.getComponent(UIDimensionComponent.class) == null) throw new IllegalArgumentException();
                if(e.getComponent(UIPositionComponent.class) == null)  throw new IllegalArgumentException();
                renderChilds(e, cpos, cdim);
            }
        }

    }


    private void renderField(float x, float y, float dx, float dy, Color c){
        ShapeRenderer sr = new ShapeRenderer();
        sr.setAutoShapeType(true);
        sr.setColor(c);
        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.rect(x, y, dx, dy);
        sr.end();
    }

    private void renderBorder(float x, float y, float dx, float dy, Color c){
        ShapeRenderer sr = new ShapeRenderer();
        sr.setAutoShapeType(true);
        sr.setColor(c);
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.rect(x, y, dx, dy);
        sr.end();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        batch.begin();
        Color c;
        if(canvas.getComponent(UIBackgroundComponent.class) != null)
            c = canvas.getComponent(UIBackgroundComponent.class).backgroundColor;
        else
            c = Color.BLACK;

        Gdx.graphics.getGL20().glClearColor(c.r/100.0f, c.g/100.0f, c.b/100.0f, c.a/100.0f);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        renderChilds(canvas, new Vector2(0,0), new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        batch.end();
    }
}
